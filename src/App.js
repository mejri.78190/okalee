import { BrowserRouter, Route, Routes } from "react-router-dom";
import Contact from "./pages/Contact";
import Tarifs from "./pages/Tarifs";
import Connexion from "./pages/Connexion";
import Notfound from "./pages/Notfound";
import Accueil from "./pages/Accueil";
import Fonctionnalite from "./pages/Fonctionnalite";



function App() {
  return (
    <BrowserRouter>

    <Routes>
      <Route path='/' element={ <Accueil />} />
      <Route path='/fonctionnalite' element={ <Fonctionnalite />} />
      <Route path='/tarifs' element={<Tarifs />}/>
      <Route path='/contact' element={<Contact />}/>
      <Route path='/connexion' element={ <Connexion />}/> 
      <Route path='*' element={<Notfound/>} />
    </Routes>

</BrowserRouter>
  );
}

export default App;
