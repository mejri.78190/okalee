import React from 'react';
import Administ from '../components/Administ';
import Stagiaire from '../components/Stagiaire';
import Formateur from '../components/Formateur';
import Logo from '../components/Logo';

const Fonctionnalite = () => {
    return (
        <div className='fd'>
          <Logo />
          <Administ /> 
          <Stagiaire />
          <Formateur />
          <br/>
         <br/>
         <br/>
        </div>
    );
};

export default Fonctionnalite;