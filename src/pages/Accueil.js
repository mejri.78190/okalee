import React from 'react';
import Logo from '../components/Logo';
import Espaces from '../components/Espaces';
import Platef from '../components/Platef';
import TypeWriter from 'react-typewriter';

const Accueil = () => {
    return (
        <div className='acc'>
        <Logo />  
        <br/> 
        <br/> 
        <Platef />
        <TypeWriter typing={1}><h2 className='title2'>3 Espaces</h2></TypeWriter>
        <Espaces /> 
        
        </div>
    );
};

export default Accueil;