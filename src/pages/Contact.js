import React from 'react';
import Formulaire from '../components/Formulaire';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';
import TypeWriter from 'react-typewriter';


const Contact = () => {
    return (
        <div className='pagecont'>
        <Logo />
        <br/>
        <TypeWriter typing={1}><h3>Nous Contacter</h3></TypeWriter>
        <br/> 
        <Formulaire /> 

        <img className='fusee' src="./assets/img/fusee.jpg" alt=""></img>
        
        </div>

         
    );
};

export default Contact;