import React from 'react';

const Formateur = () => {
    return (
        <div className='bloc2'>
            <h2>Formateur</h2>
            <br/>
            <p className='pform'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae nemo reiciendis, itaque neque 
            ipsum nulla accusantium! Fugit error quibusdam ratione mollitia doloremque, voluptates repellat? 
            Possimus voluptate vitae veritatis commodi illo?</p>
            <img className='imgform' src="./assets/img/formateur.jpg" alt=""></img>
        </div>
        
    );
};

export default Formateur;