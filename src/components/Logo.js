import React from 'react';
import Navigation from './Navigation';

const Logo = () => {
    return (
        <div>
            <h1 className='title'>OKALEE</h1>
            <img className='icone' src="./assets/img/Icon awesome-layer-group.jpg" alt=""></img>
            <Navigation />
            
        </div>
    );
};

export default Logo;