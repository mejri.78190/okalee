import React from 'react';
import { NavLink } from 'react-router-dom';


const Navigation = () => {
    return (
        <nav className='navigation'>
        <ul>
    

            <NavLink  to="/" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Accueil</li>
            </NavLink>


            <NavLink  to="/fonctionnalite" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Fonctionnalité</li>
            </NavLink>


            
            <NavLink to="/tarifs" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Tarifs</li>
            </NavLink>
           
           
            <NavLink to="/contact" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Contact</li>
            </NavLink>
   
            <NavLink to="/connexion" className={ (nav) => (nav.isActive ? "nav-active" : "")   }>
            <li>Connexion</li>
            </NavLink>

        </ul>
        </nav>

    );
};

export default Navigation;