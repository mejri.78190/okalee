import React from 'react';
import { HiPhone } from "react-icons/hi";
import { FaTabletAlt } from "react-icons/fa";
import { GrFormLocation } from "react-icons/gr";

const Formulaire = () => {
    return (
        <form>
        <div className='container'>
        <br/>
        <div className='formulaire'>
        <h4 className='titleform'>Adresse</h4>
            <p><GrFormLocation className='lieu'/>15 rue des Halles 75001 Paris</p>
            <p><HiPhone className='tel'/> 07.61.05.25.62</p>
            <p><FaTabletAlt className='telfix'/> 01.45.25.12.30</p>
            
        <br/>
        
         
        <label  className='reponse' htmlFor='nom'>Votre Nom</label>
        <input className='reponse' type="texte" id='nom' name= "nom"/>
        <br></br>
        <label className='reponse' htmlFor='prenom'>Votre Prénom</label>
        <input className='reponse' type="texte" id='prenom' name= "prenom"/>

        <br></br>
        <label className='reponse' htmlFor='email'>Votre Email</label>
        <input className='reponse' type="texte" id='email' name= "email"/>

        <br/>

        <label className='reponse' htmlFor='message'>Votre Message :</label>
        <input className='reponsetxt' type="texte" id='message' name= "message"/>
        </div>
        <button className='envoyer'  type='submit' value="" onClick=""> Envoyer</button>
        

        <div>
     
        
        <img className='imgfor' src="./assets/img/paris.jpg" alt=""></img>
        </div>
        <br></br>
        </div>
        </form>  

        
       
    );
};

export default Formulaire;