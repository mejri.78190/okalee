import React from 'react';

const Platef = () => {
    return (
        <div className='stitre'>
         <img className='fusent' src="./assets/img/fusee.jpg" alt=""></img>
        <br/>
            <h1>Plateforme de gestion <br/>
            pour les centres de formations</h1>
            <p className="p1"> La <strong>solution de gestion</strong> OKALEE vous permet d'être conforme à <br/>l'exigence du Qualiopi et au suivi d'audit.</p>
            <p className="p2">Piloter l'ensemble des acteurs de votre centre : formateurs, stagiaires, équipes.</p>
            <button className='demo' type='submit' value="" onClick=""><strong>Voir la Démo</strong></button>
           
        </div>
    );
};

export default Platef;