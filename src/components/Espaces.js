import React from 'react';
import {  FaRegSun, FaUserFriends, FaUserGraduate  } from "react-icons/fa";

const Espaces = () => {

    return (
        

<div className='conteneur'>
   
            
            
        <div className='administration'>
        <FaRegSun className='ad'/>
        <h2 className='titrepart'>Administration</h2>
        <p className='p3'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, necessitatibus libero. 
        Voluptatibus, a. Maiores, delectus, consequuntur dolor quo consectetur </p>
       <button type='submit' value="" onClick="">Découvrir</button>
       </div>

        <div className='stagiaire' >
        <FaUserFriends className='st' />
        <h2 className='titrepart'>Stagiaire</h2>
        <p className='p3'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, necessitatibus libero. 
        Voluptatibus, a. Maiores, delectus, consequuntur dolor quo consectetur </p>
        <button type='submit' value="" onClick="">Découvrir</button>
        </div>

        <div className='formateur' >
         <FaUserGraduate className='ft'/>
       <h2 className='titrepart'>Formateur</h2>
        <p className='p3' >Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, necessitatibus libero. 
        Voluptatibus, a. Maiores, delectus, consequuntur dolor quo consectetur </p>
         <button type='submit' value="" onClick="">Découvrir</button> 
        </div>

</div>

    );
};

export default Espaces;